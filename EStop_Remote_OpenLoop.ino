const int EStopSwitch = 6;  // pin
const int EStopGreen = 2;  // pin
const int EStopRed = 3;  // pin
const int Buzzer = 5;  // pin
const int Battery = 23;  // pin
const float VoltConv = 1.0 / 1024.0 * 3.3 * 2;  // volts
const float LowVolt = 3.3;  // volts
const int BeepPeriod = 10 * 1000;  // milliseconds
const int BeepFreq = 4000;  // Hz
const int BeepLength = 100;  // milliseconds
unsigned long lastBeep = 0;  // milliseconds
const int LoopDelay = 20;  // milliseconds
const int BaudRate = 9600;

void setup()  {
  Serial1.begin(BaudRate);
  pinMode(EStopSwitch, INPUT);
  pinMode(EStopGreen, OUTPUT);
  pinMode(EStopRed, OUTPUT);
  pinMode(Buzzer, OUTPUT);
  pinMode(Battery, INPUT);  }

void loop()  {
  CheckEStop();
  CheckBattery();
  delay(LoopDelay);  }

void CheckEStop()  {
  if (digitalRead(EStopSwitch) == HIGH)  {
    Serial1.print('H');
    digitalWrite(EStopGreen, HIGH);
    digitalWrite(EStopRed, LOW);  }
  else  {
    Serial1.print('L');
    digitalWrite(EStopGreen, LOW);
    digitalWrite(EStopRed, HIGH);  }  }

void CheckBattery()  {
  if (analogRead(Battery) * VoltConv < LowVolt)  {
    if (millis() - lastBeep >= BeepPeriod)  {
      tone(Buzzer,BeepFreq,BeepLength);
      lastBeep = millis();  }  }  }

const int Relay = 2;  // pin
const int BaudRate = 9600;
char Message = 'a';

void setup() {
  Serial1.begin(BaudRate);
  pinMode(Relay, OUTPUT);
  digitalWrite(Relay, LOW);  }

void loop() {
  while (Serial1.available() > 0)  {
    Message = Serial1.read();
    if (Message == 'H')  {
      digitalWrite(Relay, HIGH);  }
    else  {
      digitalWrite(Relay, LOW);  }  }  }

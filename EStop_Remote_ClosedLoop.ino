// EStop_Remote_ClosedLoop

const int GreenLEDPin = 2;
const int RedLEDPin = 3;
//const int RSSIPin = 4;
const int BuzzerPin = 5;
const int SwitchPin = 6;
//const int CTSPin = 7;
const int ConnectionLEDPin = 9;
const int BatteryPin = 23;

const int BaudRate = 9600;
int LoopDelay = 0;  // milliseconds (ms)
const int ConnectedDelay = 25;  // ms
const int DisconnectedDelay = 500;  // ms

char NewMessage = ' ';
unsigned long LastMessage = 0;	// ms
unsigned long MessageTimeout = 1000;  // ms

const float VoltConv = 1.0 / 1024.0 * 3.3 * 2;  // volts
const float LowVolt = 3.3;  // volts
const int BeepFreq = 4000;  // Hz
const int BeepPeriod = 10 * 1000;  // ms
const int BeepLength = 100;  // ms
unsigned long LastBeep = 0;  // ms

void setup()
{
  pinMode(SwitchPin, INPUT);
  pinMode(BatteryPin, INPUT);
  
  pinMode(GreenLEDPin, OUTPUT);
  pinMode(RedLEDPin, OUTPUT);
  pinMode(ConnectionLEDPin, OUTPUT);
  pinMode(BuzzerPin, OUTPUT);
  
  Serial1.begin(BaudRate);
  Serial1.print("test");
}

void loop()
{
  UpdateLEDs();
  TransmitSwitchPosition();
  CheckBattery();
  delay(LoopDelay);
}

void UpdateLEDs()
{
  NewMessage = CheckForMessage();
  
  if (NewMessage == 'C')
  {
    UpdateLED('G', 255);
    LoopDelay = ConnectedDelay;
  }
  else if (NewMessage == 'O')
  {
    UpdateLED('R', 255);
    LoopDelay = ConnectedDelay;
  }
  else
  {
    if(millis() - LastMessage < MessageTimeout && LoopDelay != 0)
    {
      UpdateLED(' ', 255 * (LastMessage + MessageTimeout - millis()) / MessageTimeout);
    }
    else
    {
      UpdateLED('R', 0);
      LoopDelay = DisconnectedDelay;
    }
  }
}

void TransmitSwitchPosition()
{
  if (digitalRead(SwitchPin) == HIGH)
  {
    Serial1.print('C');
  }
  else
  {
    Serial1.print('O');
  }
}

void CheckBattery()
{
  if (analogRead(BatteryPin) * VoltConv < LowVolt)
  {
    if (millis() - LastBeep >= BeepPeriod)
    {
      tone(BuzzerPin, BeepFreq, BeepLength);
      LastBeep = millis();
    }
  }
}

char CheckForMessage()
{
  char ReceivedMessage = ' ';

  while (Serial1.available() > 0)
  {
    ReceivedMessage = Serial1.read();
    LastMessage = millis();
  }

  return ReceivedMessage;
}

void UpdateLED(char StatusLEDColor, int ConnectionLEDDuty)
{
  if (StatusLEDColor == 'G')
  {
    digitalWrite(RedLEDPin, LOW);
    digitalWrite(GreenLEDPin, HIGH);
  }
  else if (StatusLEDColor == 'R')
  {
    digitalWrite(GreenLEDPin, LOW);
    digitalWrite(RedLEDPin, HIGH);
  }
  
  analogWrite(ConnectionLEDPin, ConnectionLEDDuty);
}

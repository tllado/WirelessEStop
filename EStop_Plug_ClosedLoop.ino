// EStop_Plug_ClosedLoop

const int RelayPin = 2;

const int BaudRate = 9600;
int LoopDelay = 0;  // milliseconds (ms)
const int ConnectedDelay = 25;  // ms
const int DisconnectedDelay = 500;  // ms

char NewMessage = ' ';
unsigned long LastMessage = 0;  // ms
unsigned long MessageTimeout = 1000;  // ms

void setup()
{
  pinMode(RelayPin, OUTPUT);
  digitalWrite(RelayPin, LOW);
  Serial1.begin(BaudRate);Serial.begin(BaudRate);
}

void loop()
{
  NewMessage = CheckForMessage();
  
  if (NewMessage == 'C')
  {
    digitalWrite(RelayPin, HIGH);
    Serial1.print('C');Serial.print('C');
    LoopDelay = ConnectedDelay;
  }
  else if (NewMessage == 'O')
  {
    digitalWrite(RelayPin, LOW);
    Serial1.print('O');Serial.print('O');
    LoopDelay = ConnectedDelay;
  }
  else if (millis() - LastMessage > MessageTimeout)
  {
    digitalWrite(RelayPin, LOW);
    Serial1.print(' ');Serial.print('X');
    LoopDelay = DisconnectedDelay;
  }

  delay(LoopDelay);
}

char CheckForMessage()
{
  char ReceivedMessage = ' ';

  while (Serial1.available() > 0)
  {
    ReceivedMessage = Serial1.read();
    LastMessage = millis();
  }

  return ReceivedMessage;
}
